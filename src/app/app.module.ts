import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContatosComponent } from './app/contatos/contatos.component';
import { InicioComponent } from './app/inicio/inicio.component';
import { ProjetosComponent } from './app/projetos/projetos.component';
import { SobreComponent } from './app/sobre/sobre.component';

@NgModule({
  declarations: [
    AppComponent,
    ContatosComponent,
    InicioComponent,
    ProjetosComponent,
    SobreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
