import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContatosComponent } from './app/contatos/contatos.component';
import { ProjetosComponent } from './app/projetos/projetos.component';
import { SobreComponent } from './app/sobre/sobre.component';
import { InicioComponent } from './app/inicio/inicio.component';

@NgModule({
  imports: [RouterModule.forRoot([
    { path: 'inicio', component: InicioComponent},
    { path: 'contatos', component: ContatosComponent },
    { path: 'projetos', component: ProjetosComponent },
    { path: 'sobre', component: SobreComponent },
    { path: '', redirectTo: 'inicio', pathMatch: 'full' }
  ])
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
